﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Group2_Test
{
    public class LoaiXe
    {
        public string ID;
        public string Ten;
        public string MoTa;
        public LoaiXe(string id, string ten, string mota)
        {
            this.ID = id;
            this.Ten = ten;
            this.MoTa = mota;
        }
        public LoaiXe(){
            ID = "";
            Ten = "";
            MoTa = "";
        }
    }
    public class DatabaseLoaiXe
    {
        private static String connString = @"Data Source=DESKTOP-ACOOQ0G\SQLEXPRESS;Initial Catalog=DbPratice;Integrated Security=True";
        
        public static LoaiXe GetLoaiXeByID(string IDLoaiXe)
        {
            LoaiXe ketQua = new LoaiXe();
            SqlConnection connection = new SqlConnection(connString);
            try
            {
                //Mo ket noi
                connection.Open();

                //Chuan bi cau lenh query viet bang SQL
                String sqlQuery = @"SELECT [LOAI_XE_ID],[TEN_LOAI_XE],[MO_TA] FROM [DbPratice].[dbo].[Nhom02_LoaiXe] WHERE LOAI_XE_ID='"+ IDLoaiXe + "'";

                //Tao mot Sqlcommand de thuc hien cau lenh truy van da chuan bi voi ket noi hien tai
                SqlCommand command = new SqlCommand(sqlQuery, connection);

                //Thuc hien cau truy van va nhan ve mot doi tuong reader ho tro do du lieu
                SqlDataReader reader = command.ExecuteReader();

                //Su dung reader de doc tung dong du lieu
                //va thuc hien thao tac xu ly mong muon voi du lieu doc len
                while (reader.HasRows)//con dong du lieu thi doc tiep
                {
                    if (reader.Read() == false) return ketQua;//doc ko duoc thi return

                    ketQua.ID = reader.GetString(0);
                    ketQua.Ten = reader.GetString(1);
                    ketQua.MoTa = reader.GetString(2);

                }


            }
            catch (InvalidOperationException ex)
            {
                //xu ly khi ket noi co van de
                Console.WriteLine("Khong the mo ket noi hoac ket noi da mo truoc do");
            }
            catch (Exception ex)
            {
                //xu ly khi ket noi co van de
                Console.WriteLine("Ket noi xay ra loi hoac doc du lieu bi loi");
            }
            finally
            {
                //Dong ket noi sau khi thao tac ket thuc
                connection.Close();
                
            }

            return ketQua;
        }

        
        public static LoaiXe GetLoaiXeByName(string Name)
        {
            LoaiXe ketQua = new LoaiXe();
            SqlConnection connection = new SqlConnection(connString);
            try
            {
                //Mo ket noi
                connection.Open();

                //Chuan bi cau lenh query viet bang SQL
                String sqlQuery = @"SELECT [LOAI_XE_ID],[TEN_LOAI_XE],[MO_TA] FROM [DbPratice].[dbo].[Nhom02_LoaiXe] WHERE TEN_LOAI_XE=N'" + Name + "'";

                //Tao mot Sqlcommand de thuc hien cau lenh truy van da chuan bi voi ket noi hien tai
                SqlCommand command = new SqlCommand(sqlQuery, connection);

                //Thuc hien cau truy van va nhan ve mot doi tuong reader ho tro do du lieu
                SqlDataReader reader = command.ExecuteReader();

                //Su dung reader de doc tung dong du lieu
                //va thuc hien thao tac xu ly mong muon voi du lieu doc len
                while (reader.HasRows)//con dong du lieu thi doc tiep
                {
                    if (reader.Read() == false) return ketQua;//doc ko duoc thi return

                    ketQua.ID = reader.GetString(0);
                    ketQua.Ten = reader.GetString(1);
                    ketQua.MoTa = reader.GetString(2);

                }


            }
            catch (InvalidOperationException ex)
            {
                //xu ly khi ket noi co van de
                Console.WriteLine("Khong the mo ket noi hoac ket noi da mo truoc do");
            }
            catch (Exception ex)
            {
                //xu ly khi ket noi co van de
                Console.WriteLine("Ket noi xay ra loi hoac doc du lieu bi loi");
            }
            finally
            {
                //Dong ket noi sau khi thao tac ket thuc
                connection.Close();

            }

            return ketQua;
        }
    }
}
