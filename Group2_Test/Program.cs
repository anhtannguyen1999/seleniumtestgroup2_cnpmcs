﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace Group2_Test
{
    class Program
    {
        static ChromeDriver driver = new ChromeDriver();
        static void Main(string[] args)
        {
            Log("Login");
            Login("admin","10011999");
            TestXemLoaiXe();
            TestSuaLoaiXe();
            TestThemLoaiXe();
            TestXoaLoaiXe();
            Log("TEST DONE!");
        }
        
        static bool Login(string tk, string mk)
        {
            driver.Manage().Window.Maximize();
            driver.Url = "http://localhost:4200/account/login";
            driver.Navigate();
            
            //trong vong 6 giay khong load duoc la fail
            for(int i = 0; i < 600; i++)
            {
                try
                {
                    var username = driver.FindElementsByName("userNameOrEmailAddress");
                    username[0].SendKeys(tk);
                    break;
                }
                catch { }
                Thread.Sleep(100);
            }
            try
            {
                var pass = driver.FindElement(By.Name("password"));
                pass.SendKeys(mk);
                var btnLogin = driver.FindElement(By.XPath("//button[@type='submit']"));
                btnLogin.Click();
                Thread.Sleep(3000);
            }
            catch {
                Log("***FAILD: Login that bai");
                return false;
            }

            return true;
        }
        
        static void TestXemLoaiXe()
        {
            Log("Test G2_001: XemLoaiXe");

            try
            {
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/Nhom02_LoaiXe0000001");
                Thread.Sleep(2000);
                var title = driver.FindElement(By.XPath("//label[contains(.,'Chi tiết loại xe')]"));
                if (title != null)
                {
                    Log("PASS G2_001_1: Truy cap duoc vao trang chi tiet loai xe id= Nhom02_LoaiXe0000001");
                }
            }
            catch
            {
                Log("***FAILD G2_001_1: Khong truy cap duoc vao trang chi tiet loai xe id= Nhom02_LoaiXe0000001");
            }

            try
            {
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/manaykhongtontai");
                Thread.Sleep(4000);
                var title = driver.FindElement(By.XPath("//label[contains(.,'Chi tiết loại xe')]"));
                if (title != null)
                {
                    Log("***FAILD G2_001_2: Truy cap duoc vao trang khong ton tai! http://localhost:4200/app/admin/loai-xe-view/manaykhongtontai");
                }
            }
            catch
            {
                Log("PASS G2_001_2: Khong truy cap duoc vao trang khong ton tai");
            }

        }

        static void TestSuaLoaiXe()
        {
            Log("Test G2_002: SuaLoaiXe");
            try
            {
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/Nhom02_LoaiXe0000001");
                Thread.Sleep(2000);
                //Click button sua
                driver.FindElement(By.XPath("//legend/div/button[2]/i")).Click();
                Thread.Sleep(500);

                //Sau khi click button sua thi 1 so thu bi disable, enable
                //Textbox ID loai xe phai disable
                var tbxID = driver.FindElement(By.XPath("//input[@name='id']"));
                if (tbxID.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("PASS G2_002: Khong duoc chinh sua ID loai xe");
                }
                else if (tbxID.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("***FAILD G2_002: Chinh sua duoc ID loai xe");
                }
                //Textbox ten phai enable
                var tbxName = driver.FindElement(By.XPath("//input[@name='name']"));
                if (tbxName.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("***FAILD G2_002: Khong enable tbx Ten loai xe");
                }
                else if (tbxName.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("PASS G2_002: Enable tbx Ten loai xe");
                }
                //Textbox mo ta phai enable
                var tbxDescription = driver.FindElement(By.XPath("//input[@name='description']"));
                if (tbxDescription.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("***FAILD G2_002: Khong enable tbx mo ta");
                }
                else if (tbxDescription.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("PASS G2_002: Enable tbx mo ta");
                }


                #region btn
                /*
                //button sua
                var btnSua = driver.FindElement(By.XPath("//legend/div/button[2]/i"));

                if (btnSua.GetAttribute("ng-reflect-is-disabled") == "false") 
                {
                    Log("***FAILD G2_002: Khi nhan sua ma button sua khong disable");
                }
                else if (btnSua.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("PASS G2_002: Button sua da duoc disable");
                }
                //button xoa
                var btnXoa = driver.FindElement(By.XPath("//button[4]/i"));
                if (btnXoa.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("***FAILD G2_002: Khi nhan sua ma button xoa khong disable");
                }
                else if (btnXoa.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("PASS G2_002: Button xoa da duoc disable");
                }
                //button luu
                var btnLuu = driver.FindElement(By.XPath("//button[3]/i"));
                if (btnLuu.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("***FAILD G2_002: Nut luu chua duoc enable");
                }
                else if (btnLuu.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("PASS G2_002: Nut luu da duoc enable");
                }

                
                //button huy
                var btnHuy = driver.FindElement(By.XPath("//button[5]/i"));
                if (btnHuy.GetAttribute("ng-reflect-is-disabled") == "true")
                {
                    Log("***FAILD G2_002: Nut huy chua duoc enable");
                }
                else if (btnHuy.GetAttribute("ng-reflect-is-disabled") == "false")
                {
                    Log("PASS G2_002: Nut huy da duoc enable");
                }
                */
                #endregion

            }
            catch
            {
                Log("***FAILD G2_002: Can not test");
            }

            //Nhap cac testcase
            #region G2_002_1
            try
            {
                string ten = "Loại xe 1";
                string mota = "Mô tả 1";
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/Nhom02_LoaiXe0000001");
                Thread.Sleep(2000);
                //Click button sua
                driver.FindElement(By.XPath("//legend/div/button[2]/i")).Click();
                Thread.Sleep(500);
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                Thread.Sleep(500);
                var btnLuu = driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]"));
                string btnLuuPath = "/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]";
                driver.ExecuteScript("path='"+btnLuuPath+"'");
                driver.ExecuteScript("document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();");
                Thread.Sleep(500);
                LoaiXe kq = DatabaseLoaiXe.GetLoaiXeByID("Nhom02_LoaiXe0000001");
                Thread.Sleep(500);
                if(kq.ID== "Nhom02_LoaiXe0000001"&& kq.Ten==ten && kq.MoTa == mota)
                {
                    Log("PASS G2_002_1: Sua thanh cong");
                }
                else
                {
                    Log("***FAILD G2_002_1: Sua that bai");
                }
            }
            catch (Exception ex)
            {
                Log("***FAILD G2_002_1: Can not test");
               
                
            }
            #endregion

            //
            try
            {
                string specialChar = "&<>\"'`";
                string ten = "Loại xe 1 ";
                string mota = "Mô tả 1 ";
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/Nhom02_LoaiXe0000001");
                
                Thread.Sleep(2000);
                //Click button sua
                driver.FindElement(By.XPath("//legend/div/button[2]/i")).Click();
                Thread.Sleep(500);
                //G2_002_2
                foreach (char c in specialChar.ToCharArray())
                {
                    string tenTmp = ten + c.ToString();
                    driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(tenTmp);
                    Thread.Sleep(200);
                    if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                    {
                        Log("***Faild G2_002_2: Luu duoc ten chua ky tu dac biet "+tenTmp);
                    }
                    else
                    {
                        Log("PASS G2_002_2: Khong luu duoc ten chua ky tu dac biet " + tenTmp);
                    }
                    driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                    
                    //Mo ta
                    Thread.Sleep(200);
                    string moTaTmp = mota + c.ToString();
                    driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(moTaTmp);
                    Thread.Sleep(200);
                    if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                    {
                        Log("***Faild G2_002_2: Luu duoc mo ta chua ky tu dac biet " + moTaTmp);
                    }
                    else
                    {
                        Log("PASS G2_002_2: Khong luu duoc mo ta chua ky tu dac biet " + moTaTmp);
                    }
                    driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);

                }

                //G2_002_3
                #region G2_002_3
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys("a");
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(Keys.Backspace);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_002_3: Luu duoc ten rong");
                }
                else
                {
                    Log("PASS G2_002_3: Khong luu duoc ten rong");
                }
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                #endregion

                //G2_002_4
                #region G2_002_4
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys("a");
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(Keys.Backspace);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_002_4: Luu duoc mo ta rong");
                }
                else
                {
                    Log("PASS G2_002_4: Khong luu duoc mo ta rong");
                }
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                #endregion

                //G2_002_5
                #region G2_002_5
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                string ten101KyTu = "101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 1";
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten101KyTu);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_002_5: Luu duoc ten qua 100 ky tu");
                }
                else
                {
                    Log("PASS G2_002_5: Khong luu duoc ten qua 100 ky tu");
                }
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                #endregion

                //G2_002_6
                #region G2_002_6
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                string mota1001KyTu = "1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự1";
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota1001KyTu);
                Thread.Sleep(200);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_002_6: Luu duoc mo ta hon 1000 ky tu");
                }
                else
                {
                    Log("PASS G2_002_6: Khong luu duoc mo ta hon 1000 ky tu");
                }
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                #endregion

                //G2_002_7
                #region G2_002_7
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[5]")).Enabled)
                    Log("PASS G2_002_7: Nut Huy ok");
                else
                    Log("***FAILD G2_002_7: Nut huy khong hoat dong");
                #endregion
            }
            catch (Exception ex)
            {
                Log("***FAILD G2_002_2: Can not test");
            }
        }

        private static LoaiXe loaiXeVuaThem = null;
        static void TestThemLoaiXe()
        {
            #region G2_004_1
            try
            {
                DateTime dateTime = DateTime.Now;
                string date = dateTime.Year + "" + dateTime.Month + "" + dateTime.Day + "" + dateTime.Hour + "" + dateTime.Minute + "" + dateTime.Second;
                string ten = "Loại xe "+ date;
                string mota = "Mô tả "+ date;
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-add");
                Thread.Sleep(2000);
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                Thread.Sleep(500);
                string btnLuuPath = "/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]";
                driver.ExecuteScript("path='" + btnLuuPath + "'");
                driver.ExecuteScript("document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();");
                Thread.Sleep(500);
                LoaiXe kq = DatabaseLoaiXe.GetLoaiXeByName(ten);
                Thread.Sleep(500);
                if (kq.Ten == ten && kq.MoTa == mota)
                {
                    Log("PASS G2_004_1: Them thanh cong");
                    loaiXeVuaThem = kq;
                }
                else
                {
                    Log("***FAILD G2_004_1: Them that bai");
                }
            }
            catch (Exception ex)
            {
                Log("***FAILD G2_004_1: Can not test");


            }
            #endregion

            #region G2_004_2
            try
            {
                string specialChar = "&<>\"'`";
                string ten = "Loại xe 1 ";
                string mota = "Mô tả 1 ";
                driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-add");
                Thread.Sleep(2000);
                //G2_004_2
                foreach (char c in specialChar.ToCharArray())
                {
                    string tenTmp = ten + c.ToString();
                    driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(tenTmp);
                    Thread.Sleep(200);
                    if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                    {
                        Log("***Faild G2_004_2: Luu duoc ten chua ky tu dac biet " + tenTmp);
                    }
                    else
                    {
                        Log("PASS G2_004_2: Khong luu duoc ten chua ky tu dac biet " + tenTmp);
                    }
                    driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);

                    //Mo ta
                    Thread.Sleep(200);
                    string moTaTmp = mota + c.ToString();
                    driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(moTaTmp);
                    Thread.Sleep(200);
                    if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                    {
                        Log("***Faild G2_004_2: Luu duoc mo ta chua ky tu dac biet " + moTaTmp);
                    }
                    else
                    {
                        Log("PASS G2_004_2: Khong luu duoc mo ta chua ky tu dac biet " + moTaTmp);
                    }
                    driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                    driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);

                }

                //G2_004_3
                #region G2_004_3
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys("a");
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(Keys.Backspace);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_004_3: Luu duoc ten rong");
                }
                else
                {
                    Log("PASS G2_004_3: Khong luu duoc ten rong");
                }
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                #endregion

                //G2_004_4
                #region G2_004_4
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys("a");
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(Keys.Backspace);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_004_4: Luu duoc mo ta rong");
                }
                else
                {
                    Log("PASS G2_004_4: Khong luu duoc mo ta rong");
                }
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                #endregion

                //G2_002_5
                #region G2_004_5
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                string ten101KyTu = "101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 101 ký tự 1";
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten101KyTu);
                Thread.Sleep(100);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_004_5: Luu duoc ten qua 100 ky tu");
                }
                else
                {
                    Log("PASS G2_004_5: Khong luu duoc ten qua 100 ky tu");
                }
                driver.FindElement(By.XPath("//input[@name='name']")).Clear();
                driver.FindElement(By.XPath("//input[@name='name']")).SendKeys(ten);
                #endregion

                //G2_004_6
                #region G2_004_6
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                string mota1001KyTu = "1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự 1001 ký tự1";
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota1001KyTu);
                Thread.Sleep(200);
                if (driver.FindElement(By.XPath("/html/body/app-root/ng-component/div/div/div[2]/ng-component/html/body/div/div/div/div/form/legend[1]/div/button[3]")).Enabled)
                {
                    Log("***Faild G2_004_6: Luu duoc mo ta hon 1000 ky tu");
                }
                else
                {
                    Log("PASS G2_004_6: Khong luu duoc mo ta hon 1000 ky tu");
                }
                driver.FindElement(By.XPath("//input[@name='description']")).Clear();
                driver.FindElement(By.XPath("//input[@name='description']")).SendKeys(mota);
                #endregion

            }
            catch
            {
                Log("***Faild G2_004: Khong thuc hien duoc");
            }
            #endregion

        }
        static void TestXoaLoaiXe()
        {
            //loaiXeVuaThem = new LoaiXe("Nhom02_LoaiXe0000008", "","");
            if (DatabaseLoaiXe.GetLoaiXeByID(loaiXeVuaThem.ID).ID == "")
            {
                Log("***FAILD G2_003_1: Chua xac dinh loai xe can xoa!");
                return;
            }
            driver.Navigate().GoToUrl("http://localhost:4200/app/admin/loai-xe-view/"+ loaiXeVuaThem.ID+ "");
            Thread.Sleep(2000);
            //Click button xoa
            driver.FindElement(By.XPath("//legend/div/button[4]/i")).Click();
            string btnXacNhanXpath = "/html/body/div/div/div[4]/div[2]/button";
            driver.ExecuteScript("path='" + btnXacNhanXpath + "'");
            driver.ExecuteScript("document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();");
            Thread.Sleep(500);
            if (DatabaseLoaiXe.GetLoaiXeByID(loaiXeVuaThem.ID).ID == loaiXeVuaThem.ID)
            {
                Log("***Faild G2_003_2: Xoa that bai!");
            }
            else
                Log("Pass G2_003_2: Xoa thanh cong!");
        }

        static void Log(string txt)
        {
            Console.WriteLine(txt);
            //writeToFileWithUTF8("Group2_Test.txt",txt);
        }
        static void writeToFileWithUTF8(string filePath, string txt)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Append))
            {
                using (StreamWriter writer = new StreamWriter(fs, Encoding.UTF8))
                {
                    writer.WriteLine(txt);
                }
            }
        }
    }
}
